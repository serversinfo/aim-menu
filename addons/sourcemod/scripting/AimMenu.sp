/***************************************************************
		Special Thanks to Scipizoa for the Idea
	(https://forums.alliedmods.net/showthread.php?t=206780)
***************************************************************/

#include <sourcemod>
#include <sdktools>
#include <clientprefs>
//#include <morecolors>
#pragma semicolon 1

#define TEAM_SPECTATOR 1
#define VERSION "2.0.35"

Handle configFile = INVALID_HANDLE;

int targetname[MAXPLAYERS+1];

char BanString[255];

enum
{
	Kick = 0,
	Ban,
	EnumSize
};

public Plugin:myinfo = 
{
	name		= "Aim Menu",
	author		= "Skipper",
	description	= "Admins Commands @Aim on the fly",
	version		= VERSION,
	url			= "http://steamcommunity.com/id/Skipperz/"
}

public OnPluginStart()
{
	RegAdminCmd("sm_aim", Command_MainAimMenu, ADMFLAG_SLAY);
	AddCommandListener(Command_buyammo1, "buyammo1");
	RegAdminCmd("sm_reloadaim", Command_ReloadAimConfig, ADMFLAG_SLAY);
	LoadConfig();
}

public Action:ButtonChecker(Handle timer)
{
	for (int i=1; i<=MaxClients; i++)
		if (IsClientInGame(i))
			if (GetClientButtons(i) & IN_USE)
				ExecuteMenu(i);
	return Plugin_Continue;
}

public Action Command_buyammo1(int client, const char[] command, int argc)
{
	ExecuteMenu(client);
}
	
LoadConfig()
{
	configFile = CreateKeyValues("Aim Menu");
	decl String:path[64];
	BuildPath(Path_SM, path, sizeof(path), "configs/AimMenu.cfg");
	if(!FileToKeyValues(configFile, path)) 
		SetFailState("Aim Menu Config file missing");
}

public Action Command_ReloadAimConfig(client, args) 
{
	LoadConfig();
	ReplyToCommand(client, "Aim Menu Config has been refreshed.");
	return Plugin_Handled;
}

public Action Command_MainAimMenu(client, args)
{
	if(IsClientInGame(client))
		ExecuteMenu(client);
	else ReplyToCommand(client, "You must be ingame to access this command.");
	return Plugin_Handled;	
}

public ExecuteMenu(client)
{
	if(!IsClientInGame(client))
		return;
	Handle menu = CreateMenu(ExecuteCallback);
	int spectator = GetEntPropEnt(client,Prop_Send,"m_hObserverTarget");
	int targetaim = GetClientAimTarget(client, true);
		
	targetname[client] = (GetClientTeam(client) == TEAM_SPECTATOR) ? spectator:targetaim;
	if(targetname[client] <= -1) {
		PrintToChat(client, "[Aim Menu] Цель не найдена.");
		return;
	} else {	
		decl String:name[32];
		GetClientName(targetname[client], name, sizeof(name));
	
		decl String:Auth[21];
		GetClientAuthId(targetname[client], AuthId_Steam2, Auth, sizeof(Auth));	
	
		SetMenuTitle(menu, "Игрок: %s [%s]", name, Auth);
			
		if(CheckCommandAccess(client, "sm_kick", ADMFLAG_KICK))  
			AddMenuItem(menu, "sm_kick", "Кикнуть");
		if(CheckCommandAccess(client, "sm_ban", ADMFLAG_SLAY))  
			AddMenuItem(menu, "sm_ban", "Забанить");
		if(CheckCommandAccess(client, "sm_mute", ADMFLAG_GENERIC))
			AddMenuItem(menu, "sm_mute", "Отключить игроку микрофон");
	
		char sCmd[32];
		char sSection[32];
		
		decl String:path[64];
	
		configFile = CreateKeyValues("Aim Menu");
		BuildPath(Path_SM, path, sizeof(path), "configs/AimMenu.cfg");
	
		if (FileToKeyValues(configFile, path)) {
			KvRewind(configFile);			
			if (KvJumpToKey(configFile, "Custom"))	 {
				KvGotoFirstSubKey(configFile);
				do {
					KvGetSectionName(configFile, sSection, sizeof(sSection));
					KvGetString(configFile, "cmd", sCmd, sizeof(sCmd));
					
					if(CheckCommandAccess(client, sCmd, ADMFLAG_GENERIC)) 
						AddMenuItem(menu, sCmd, sSection);
				} while(KvGotoNextKey(configFile));
			}	
		}
	}
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}	

public ExecuteCallback(Handle:menu, MenuAction:action, client, param2)
{
	if (action == MenuAction_End) CloseHandle(menu);
	if (action == MenuAction_Select) {
		char info[64];
		GetMenuItem(menu, param2, info, sizeof(info));
				
		if(StrEqual(info, "sm_kick"))
			AimMenu(client, Kick);
		else if(StrEqual(info, "sm_ban")) 
			AimMenu(client, Ban);
		else if(StrEqual(info, "sm_mute")) 
			FakeClientCommand(client, "sm_mute #%d", GetClientUserId(targetname[client]));
		else FakeClientCommand(client, "%s #%d", info, GetClientUserId(targetname[client]));
	}
}

public Action:AimMenu(client, id)
{
	Handle menu;

	switch(id) {
		case 0: {
			menu = CreateMenu(KickMenuCallback);
			SetMenuTitle(menu, "Причина кика");
		}
		case 1: {
			menu = CreateMenu(BanMenuCallback);
			SetMenuTitle(menu, "Причина бана");
		}
		default: return;
	}
	
	char sReason[32];
	char sSection[32];
		
	decl String:path[64];
	
	configFile = CreateKeyValues("Aim Menu");
	BuildPath(Path_SM, path, sizeof(path), "configs/AimMenu.cfg");
	
	if (FileToKeyValues(configFile, path)) {
		KvRewind(configFile);			
		if (KvJumpToKey(configFile, "Reasons"))	 {
			KvGotoFirstSubKey(configFile);
			do {
				KvGetSectionName(configFile, sSection, sizeof(sSection));
				KvGetString(configFile, "reason", sReason, sizeof(sReason));
		
				AddMenuItem(menu, sReason, sSection);
			} while(KvGotoNextKey(configFile));
		}	
	}
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}	

public KickMenuCallback(Handle:menu, MenuAction:action, client, param2)
{
	if(action == MenuAction_End) CloseHandle(menu);
	if(action == MenuAction_Cancel && param2 == MenuCancel_ExitBack) AimMenu(client, Kick);
	if(action == MenuAction_Select) {
		new String:info[16];
		GetMenuItem(menu, param2, info, sizeof(info));
		
		FakeClientCommand(client, "sm_kick #%d %s", GetClientUserId(targetname[client]), info);
	}
}	

public BanMenuCallback(Handle:menu, MenuAction:action, client, param2)
{
	if(action == MenuAction_End) CloseHandle(menu);
	if(action == MenuAction_Cancel && param2 == MenuCancel_ExitBack) AimMenu(client, Ban);
	if(action == MenuAction_Select) {
		new String:banReason[32];
		GetMenuItem(menu, param2, banReason, sizeof(banReason));	
		
		if (strlen(banReason) > 0)
			Format(BanString, sizeof(BanString), "%s %s", BanString, banReason); 
		
		TimeMenu(client);
	}
}

public Action:TimeMenu(client)
{
	new Handle:menu = CreateMenu(TimeMenuCallback);
	
	SetMenuTitle(menu, "Время");
	
	new String:sTime[16];
	new String:sSection[32];
		
	decl String:path[64];
	
	configFile = CreateKeyValues("Aim Menu");
	BuildPath(Path_SM, path, sizeof(path), "configs/AimMenu.cfg");
	
	if (FileToKeyValues(configFile, path)) {
		KvRewind(configFile);			
		if (KvJumpToKey(configFile, "BanTimes")) {
			KvGotoFirstSubKey(configFile);
			do {
				KvGetSectionName(configFile, sSection, sizeof(sSection));
				KvGetString(configFile, "time", sTime, sizeof(sTime));
		
				AddMenuItem(menu, sTime, sSection);
			} while(KvGotoNextKey(configFile));
		}	
	}

	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}	
		
public TimeMenuCallback(Handle:menu, MenuAction:action, client, param2)
{
	if(action == MenuAction_End) CloseHandle(menu);
	if(action == MenuAction_Cancel && param2 == MenuCancel_ExitBack) AimMenu(client, Ban);
	if(action == MenuAction_Select) {
		new String:BanTime[16];
		GetMenuItem(menu, param2, BanTime, sizeof(BanTime));	
		Format(BanString, sizeof(BanString), "%s %s", BanTime, BanString);
		
		FakeClientCommand(client, "sm_ban #%d %s", GetClientUserId(targetname[client]), BanString);		
	}
}